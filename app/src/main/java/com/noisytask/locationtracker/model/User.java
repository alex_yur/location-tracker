package com.noisytask.locationtracker.model;


public class User {

    private String name;

    private Location location;

    private String key;

    private String email;

    public User() {
    }

    public User(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getKey() {
        return key;
    }

    public User setKey(String key) {
        this.key = key;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        User user = (User) o;

        return key.equals(user.key);
    }

    @Override
    public int hashCode() {
        return 31 * key.hashCode();
    }
}
