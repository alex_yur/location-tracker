package com.noisytask.locationtracker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class Location {

    private double lat;

    private double lng;

    private long date;

    public Location() {
    }

    public Location(double lat, double lng, Date date) {
        this.lat = lat;
        this.lng = lng;
        this.date = date.getTime();
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Date getDate(){
        return new Date(date);
    }

    public void setDate(Date date){
        this.date = date.getTime();
    }

    @JsonIgnore
    public LatLng getLatLng(){
        return new LatLng(lat, lng);
    }
}
