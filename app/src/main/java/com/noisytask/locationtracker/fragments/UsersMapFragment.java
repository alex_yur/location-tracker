package com.noisytask.locationtracker.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.noisytask.locationtracker.R;
import com.noisytask.locationtracker.events.EventAddedUser;
import com.noisytask.locationtracker.events.EventUserClicked;
import com.noisytask.locationtracker.events.EventUserLocationChanged;
import com.noisytask.locationtracker.holders.BusHolder;
import com.noisytask.locationtracker.holders.UsersHolder;
import com.noisytask.locationtracker.model.User;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Map fragment.
 */
public class UsersMapFragment extends Fragment implements OnMapReadyCallback {

    private final static String TAG = "User Map fragment";

    SupportMapFragment supportMapFragment;
    private GoogleMap mGoogleMap;
    private Map<String, Marker> markerMap = new HashMap<>();
    private Bus bus;
    private User clickedUser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container);
        bus = BusHolder.getBus();
        bus.register(this);

        supportMapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        supportMapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        if(UsersHolder.getInstance().getUserList().size() > 0){
            addMarkers(null);
        }

        if(clickedUser != null){
            moveCamToUser(clickedUser);
        }
    }

    @Subscribe public void addMarkers(EventAddedUser eventAddedUser){
        List<User> userList = UsersHolder.getInstance().getUserList();

        for(User user : userList){
            if(!markerMap.containsKey(user.getKey()) && user.getLocation() != null){
                addMarker(user);
            }
        }

        if(userList.size() == markerMap.size() && clickedUser == null){
            mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

                @Override
                public void onCameraChange(CameraPosition arg0) {
                    focusCamera(new ArrayList<Marker>(markerMap.values()));
                    mGoogleMap.setOnCameraChangeListener(null);
                }
            });
        }

    }

    private void addMarker(final User user){
        LatLng latlng = user.getLocation().getLatLng();
        Marker marker = mGoogleMap.addMarker(
                new MarkerOptions()
                        .position(latlng)
                        .title(user.getName())
                        .snippet(getLastUpdatedText(user.getLocation().getDate())));
        markerMap.put(user.getKey(), marker);
    }

    @Subscribe public void moveMarker(EventUserLocationChanged eventUserLocationChanged){
        User user = eventUserLocationChanged.getUser();
        LatLng latlng = user.getLocation().getLatLng();

        Marker marker = markerMap.get(user.getKey());
        if(marker != null){
            marker.setSnippet(getLastUpdatedText(user.getLocation().getDate()));
            marker.setPosition(latlng);
        } else {
            marker = mGoogleMap.addMarker(
                    new MarkerOptions()
                            .position(latlng)
                            .title(user.getName())
                            .snippet(getLastUpdatedText(user.getLocation().getDate())));
            markerMap.put(user.getKey(), marker);
        }
    }

    /**
     * Move camera to clicked user.
     */
    @Subscribe public void focusUser(EventUserClicked eventUserClicked){
        if(eventUserClicked.getUser() != null){
            if(mGoogleMap != null){
                moveCamToUser(eventUserClicked.getUser());
            } else {
                clickedUser = eventUserClicked.getUser();
            }
        }
    }

    private void moveCamToUser(final User user){
        mGoogleMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(user.getLocation().getLatLng(), 17));
    }


    /**
     * Focus camera, so we can see all users.
     */
    private void focusCamera(final List<Marker> markers){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker m : markers) {
            builder.include(m.getPosition());
        }

        LatLngBounds bounds = builder.build();

        int padding = 100;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mGoogleMap.moveCamera(cu);
    }

    private String getLastUpdatedText(final Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");
        return "Last update: " + dateFormat.format(date);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bus.unregister(this);
    }
}
