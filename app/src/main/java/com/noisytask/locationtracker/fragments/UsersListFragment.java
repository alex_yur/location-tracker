package com.noisytask.locationtracker.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.noisytask.locationtracker.R;
import com.noisytask.locationtracker.activities.MainActivity;
import com.noisytask.locationtracker.activities.MapActivity;
import com.noisytask.locationtracker.adapters.UsersListAdapter;
import com.noisytask.locationtracker.events.EventAddedUser;
import com.noisytask.locationtracker.events.EventUserClicked;
import com.noisytask.locationtracker.events.EventUserLocationChanged;
import com.noisytask.locationtracker.holders.BusHolder;
import com.noisytask.locationtracker.holders.UsersHolder;
import com.noisytask.locationtracker.model.User;
import com.noisytask.locationtracker.utils.Constants;
import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;


public class UsersListFragment extends Fragment {

    private Bus bus;
    private UsersListAdapter usersListAdapter;
    private List<User> userList;

    @BindView(R.id.list_view) ListView mListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container);
        ButterKnife.bind(this, view);
        bus = BusHolder.getBus();
        bus.register(this);

        if(!UsersHolder.getInstance().isEmpty()){
            setUsersListAdapter(null);
        }

        return view;
    }

    @Subscribe public void setUsersListAdapter(EventAddedUser eventAddedUser){
        userList = UsersHolder.getInstance().getUserList();

        if(usersListAdapter == null){
            usersListAdapter = new UsersListAdapter(userList, getContext());
            mListView.setAdapter(usersListAdapter);
        } else {
            usersListAdapter.setUserList(userList);
        }
    }

    @Subscribe public void updateCurrentUser(EventUserLocationChanged eventUserLocationChanged){
        userList = UsersHolder.getInstance().getUserList();
    }

    @OnItemClick(R.id.list_view) void onListItemClick(int position){
        clickedUser = userList.get(position);
        if(((MainActivity) getActivity()).isMapFragmentAttached()){
            if(clickedUser.getLocation() != null){
                bus.post(new EventUserClicked(clickedUser));
            } else {
                Toast.makeText(getActivity(), "No User data stored yet.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Intent intent = new Intent(getActivity(), MapActivity.class);
            intent.putExtra(Constants.USER_KEY, userList.get(position).getKey());
            startActivity(intent);
        }
    }

    private User clickedUser;

    @Produce public EventUserClicked sendEvent(){
        return new EventUserClicked(clickedUser);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bus.unregister(this);
    }
}
