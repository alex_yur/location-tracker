package com.noisytask.locationtracker.events;

import com.noisytask.locationtracker.model.User;

/**
 * Launch event when user in list is clicked.
 * Used to focus clicked user on the map.
 */
public class EventUserClicked {

    private User user;

    public EventUserClicked(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
