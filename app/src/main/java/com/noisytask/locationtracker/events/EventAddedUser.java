package com.noisytask.locationtracker.events;

/**
 * Event is triggered only when new user adds to application.
 */
public class EventAddedUser {

    public EventAddedUser() {}
}
