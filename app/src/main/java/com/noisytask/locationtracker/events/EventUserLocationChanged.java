package com.noisytask.locationtracker.events;

import com.noisytask.locationtracker.model.User;

/**
 * Event is triggered when any users location is changed.
 */
public class EventUserLocationChanged {

    private User user;

    public EventUserLocationChanged(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
