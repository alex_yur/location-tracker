package com.noisytask.locationtracker.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.noisytask.locationtracker.R;
import com.noisytask.locationtracker.events.EventAddedUser;
import com.noisytask.locationtracker.events.EventUserLocationChanged;
import com.noisytask.locationtracker.firebase.FirebaseHelper;
import com.noisytask.locationtracker.gps.LocationHelper;
import com.noisytask.locationtracker.holders.BusHolder;
import com.noisytask.locationtracker.holders.UsersHolder;
import com.noisytask.locationtracker.model.User;
import com.noisytask.locationtracker.utils.Constants;
import com.squareup.otto.Bus;

/**
 * App main activity.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main Activity";

    private Bus bus;
    private Firebase firebase;
    private FirebaseHelper firebaseHelper;
    SharedPreferences sharedPreferences;
    private String userKey;

    private LocationHelper locationHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(Constants.SP, MODE_PRIVATE);
        userKey = sharedPreferences.getString(Constants.USER_KEY, null);

        bus = BusHolder.getBus();

        firebaseHelper = FirebaseHelper.getInstanse();
        firebase = firebaseHelper.getFirebase();

        firebaseHelper.setupChildChangedListener(userKey, bus);

        locationHelper = new LocationHelper(this);
        locationHelper.setupLocationListener();
    }

    /**
     * Checks if map fragment is attached to current activity.
     */
    public boolean isMapFragmentAttached(){
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.users_map_fragment);

        return fragment != null && fragment.isVisible();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationHelper.disableLocationListener();
    }
}
