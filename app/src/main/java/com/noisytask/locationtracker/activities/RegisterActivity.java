package com.noisytask.locationtracker.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.noisytask.locationtracker.R;
import com.noisytask.locationtracker.firebase.FirebaseHelper;
import com.noisytask.locationtracker.model.User;
import com.noisytask.locationtracker.utils.Constants;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Register new user.
 */
public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.name) EditText mName;
    @BindView(R.id.email) EditText mEmail;
    @BindView(R.id.submit) Button mSubmit;

    private static final String TAG = "Register Activity";

    private Firebase firebase;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(Constants.SP, MODE_PRIVATE);

    }

    @OnClick(R.id.submit) void onRegisterClick(){
        if(isValid()){
            register();
        } else {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Checks if fields are not empty.
     */
    private boolean isValid(){
        return !mName.getText().toString().equals("") && !mEmail.getText().toString().equals("");
    }

    /**
     * Checks if user with given email is already in system,
     * if so, returns key. Otherwise create new user,
     */
    private void register(){
        firebase = FirebaseHelper.getInstanse().getFirebase();

        firebase.child(Constants.USERS).orderByChild(Constants.EMAIL).equalTo(mEmail.getText().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == null){
                    createNewUser();
                } else {
                    Map map = dataSnapshot.getValue(Map.class);
                    putKeyToSPAndGoToSplashActivity(map.keySet().iterator().next().toString());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(TAG, "Error while fetching user by email occurred.");
            }
        });

    }

    /**
     * Creates new user in firebase.
     */
    private void createNewUser(){
        User newUser = new User(mName.getText().toString(), mEmail.getText().toString());
        Firebase newUserRef = firebase.child(Constants.USERS).push();
        newUserRef.setValue(newUser);
        String newUserKey = newUserRef.getKey();
        putKeyToSPAndGoToSplashActivity(newUserKey);
    }

    /**
     * Adds users key to shared preferences and launches splashscreen.
     */
    private void putKeyToSPAndGoToSplashActivity(String userKey){
        sharedPreferences.edit().putString(Constants.USER_KEY, userKey).apply();
        Intent intent = new Intent(this, SplashScreenActivity.class);
        startActivity(intent);
        finish();
    }


}
