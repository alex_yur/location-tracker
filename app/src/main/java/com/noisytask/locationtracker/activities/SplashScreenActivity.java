package com.noisytask.locationtracker.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.noisytask.locationtracker.firebase.FirebaseHelper;
import com.noisytask.locationtracker.gps.PermissionsHelper;
import com.noisytask.locationtracker.holders.UsersHolder;
import com.noisytask.locationtracker.model.User;
import com.noisytask.locationtracker.utils.Constants;

import java.util.HashMap;
import java.util.Map;

public class SplashScreenActivity extends AppCompatActivity {

    private final static int LOCATION_REQUEST_CODE = 435;
    private final static String TAG = "Splash Screen";

    SharedPreferences sharedPreferences;
    private Firebase firebase;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(Constants.SP, MODE_PRIVATE);
        firebase = FirebaseHelper.getInstanse().getFirebase();

        if(!checkLocationPermission()){
            requestPermission();
        } else {
            goToNextActivity();
        }
    }

    /**
     * If userKey is already present, fetch data and go to main,
     * otherwise go to register activity.
     */
    private void goToNextActivity(){
        String userKey = sharedPreferences.getString(Constants.USER_KEY, null);

        if(userKey == null){
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
            finish();
        } else {
            fetchUsersData(userKey);
        }
    }

    /**
     * Get available users from firebase.
     */
    private void fetchUsersData(final String userKey){
        final Dialog dialog = new ProgressDialog(this);
        dialog.setTitle("Loading...");
        dialog.show();
        firebase.child(Constants.USERS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, User> newUserMap = new HashMap<>();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    String key = ds.getKey();
                    User newUser = ds.getValue(User.class);
                    if(!userKey.equals(key)){
                        newUserMap.put(key, newUser.setKey(key));
                    }
                }
                UsersHolder.getInstance().setUserMap(newUserMap);
                dialog.dismiss();

                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


    /**
     * Requests location permission.
     */
    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
    }

    /**
     * Checks if app already have location permission.
     */
    private boolean checkLocationPermission(){
        return new PermissionsHelper(this).checkLocationPermission();
    }

    /**
     * Request permission callback.
     */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Location permission granted.");

                    goToNextActivity();
                } else {
                    finish();
                }
                return;
            }
        }
    }
}
