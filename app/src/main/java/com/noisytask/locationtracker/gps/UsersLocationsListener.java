package com.noisytask.locationtracker.gps;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.widget.Toast;

import com.noisytask.locationtracker.firebase.FirebaseHelper;
import com.noisytask.locationtracker.holders.BusHolder;
import com.noisytask.locationtracker.utils.Constants;
import com.squareup.otto.Bus;

import java.util.Date;

/**
 * Locations listener witch performs locations update on firebase to current user.
 */
public class UsersLocationsListener implements LocationListener {

    private Context context;
    private FirebaseHelper firebaseHelper;
    private String userKey;

    public UsersLocationsListener(Context context) {
        this.context = context;
        firebaseHelper = FirebaseHelper.getInstanse();
        userKey = context.getSharedPreferences(Constants.SP, Context.MODE_PRIVATE).getString(Constants.USER_KEY, null);
    }

    @Override
    public void onLocationChanged(Location location) {
        firebaseHelper.updateUsersLocation(
                new com.noisytask.locationtracker.model.Location(
                        location.getLatitude(),
                        location.getLongitude(),
                        new Date()),
                userKey);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
