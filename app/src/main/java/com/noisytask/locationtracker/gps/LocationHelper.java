package com.noisytask.locationtracker.gps;

import android.content.Context;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

import com.noisytask.locationtracker.utils.Constants;

/**
 * Location service helper class.
 * Provides location listener setup and disable methods.
 */
public class LocationHelper {

    private static final String TAG = "Location helper";

    private Context context;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private PermissionsHelper permissionsHelper;

    public LocationHelper(Context context) {
        this.context = context;
        locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);
        permissionsHelper = new PermissionsHelper(context);
        setupGPSEventsListener();
    }

    /**
     * Set up location listener.
     */
    public void setupLocationListener() {
        Log.i(TAG, "Setting up location tracker");
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (permissionsHelper.checkLocationPermission()) {
                locationListener = new UsersLocationsListener(context);

                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (location != null) {
                    locationListener.onLocationChanged(location);
                }

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        Constants.LOCATION_UPDATE_INTERVAL,
                        Constants.LOCATION_UPDATE_DISTANCE,
                        locationListener);

            } else {
                Toast.makeText(context, "Location permission is not granted.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Enable GPS on your device to use service", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Setup gps turn on/off listener.
     */
    public void setupGPSEventsListener(){
        if (permissionsHelper.checkLocationPermission()) {
            locationManager.addGpsStatusListener(new GpsStatus.Listener() {
                @Override
                public void onGpsStatusChanged(int event) {
                    switch (event) {
                        case GpsStatus.GPS_EVENT_STARTED:
                            Log.i(TAG, "GPS module turned on.");
                            setupLocationListener();
                            break;
                        case GpsStatus.GPS_EVENT_STOPPED:
                            Log.i(TAG, "GPS module turned off.");
                            disableLocationListener();
                            break;
                    }
                }
            });
        }
    }

    /**
     * Disable current location listener.
     */
    public void disableLocationListener() {
        if (permissionsHelper.checkLocationPermission()) {
            locationManager.removeUpdates(locationListener);

            Log.i(TAG, "Location manager disabled.");
        }
    }
}
