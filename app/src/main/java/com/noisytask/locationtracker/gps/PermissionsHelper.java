package com.noisytask.locationtracker.gps;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by oleksiyyur on 5/30/16.
 */
public class PermissionsHelper {

    private Context context;

    public PermissionsHelper(Context context) {
        this.context = context;
    }

    public boolean checkLocationPermission(){
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}
