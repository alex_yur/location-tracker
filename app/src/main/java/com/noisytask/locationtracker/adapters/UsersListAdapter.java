package com.noisytask.locationtracker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.noisytask.locationtracker.R;
import com.noisytask.locationtracker.model.User;

import java.util.List;

/**
 * Adapter for user's list.
 */
public class UsersListAdapter extends BaseAdapter {

    private List<User> userList;
    private Context context;

    public UsersListAdapter(List<User> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolder {
        public TextView userName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);

            view = inflater.inflate(R.layout.adapter_users_list, null, false);
            viewHolder = new ViewHolder();

            viewHolder.userName = (TextView) view.findViewById(R.id.user_name);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.userName.setText(userList.get(position).getName());


        return view;
    }

    public void setUserList(final List<User> userList) {
        this.userList = userList;
        notifyDataSetChanged();
    }
}
