package com.noisytask.locationtracker.holders;

import com.squareup.otto.Bus;

/**
 * Hold Otto's Bus instance.
 * Singleton.
 */
public class BusHolder {

    private static Bus bus;

    public static Bus getBus(){
        if(bus == null){
            bus = new Bus();
        }

        return bus;
    }

}
