package com.noisytask.locationtracker.holders;

import com.noisytask.locationtracker.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Holds map with current users available.
 * Singleton.
 */
public class UsersHolder {

    private Map<String, User> userMap = new HashMap<>();
    private static UsersHolder usersHolder;

    private UsersHolder(){}

    /**
     * Returns UserListHolder Instance.
     */
    public static UsersHolder getInstance(){
        if(usersHolder == null){
            usersHolder = new UsersHolder();
        }

        return usersHolder;
    }

    public void setUserList(final List<User> userList){
        for(User user : userList){
            userMap.put(user.getKey(), user);
        }
    }

    public void setUserMap(final Map<String, User> userMap){
        this.userMap = userMap;
    }

    public List<User> getUserList(){
        return new ArrayList<>(userMap.values());
    }

    /**
     * Checks if user map contains given user, checks by key.
     * @param user user to check.
     * @return true if user is already in map.
     */
    public boolean isUserInMap(final User user){
        return userMap.containsKey(user.getKey());
    }

    /**
     * Return true if users map is empty.
     */
    public boolean isEmpty(){
        return userMap.isEmpty();
    }

    /**
     * Adds new user to map
     * @param user user to add.
     */
    public void addUserToMap(final User user){
        userMap.put(user.getKey(), user);
    }

    /**
     * Updates given user location in map.
     * The main reason why I've chosen to use map, is that we can find user instantly by key,
     * no iteration needed.
     *
     * @param user user to update.
     */
    public void updateUser(final User user){
        User innerUser = userMap.get(user.getKey());
        innerUser.setLocation(user.getLocation());
    }
}
