package com.noisytask.locationtracker.firebase;

import android.util.Log;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.noisytask.locationtracker.events.EventAddedUser;
import com.noisytask.locationtracker.events.EventUserLocationChanged;
import com.noisytask.locationtracker.holders.UsersHolder;
import com.noisytask.locationtracker.model.Location;
import com.noisytask.locationtracker.model.User;
import com.noisytask.locationtracker.utils.Constants;
import com.squareup.otto.Bus;

public class FirebaseHelper {

    private static final String TAG = "Firebase Helper.";

    private static FirebaseHelper firebaseHelper;
    private Firebase firebase;

    private FirebaseHelper(){}

    public static FirebaseHelper getInstanse(){
        if(firebaseHelper == null){
            firebaseHelper = new FirebaseHelper();
        }

        return firebaseHelper;
    }

    public  Firebase getFirebase(){
        if(firebase == null){
            firebase = new Firebase(Constants.FIREBASE_URL);
        }

        return firebase;
    }

    /**
     * Updates location of current user on firebase.
     * @param location new location.
     * @param userKey current users Id.
     */
    public void updateUsersLocation(final Location location, final String userKey){
        firebase.child(Constants.USERS).child(userKey).child(Constants.USER_LOCATION).setValue(location);
    }

    public void setupChildChangedListener(final String userKey, final Bus bus){
        firebase.child(Constants.USERS).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User newUser = dataSnapshot.getValue(User.class).setKey(dataSnapshot.getKey());

                if(!newUser.getKey().equals(userKey)){
                    if(UsersHolder.getInstance().isUserInMap(newUser)){
                        Log.i(TAG, "User with id " + newUser.getKey() + " already in list.");
                    } else{
                        Log.i(TAG, "Adding new User with id " + newUser.getKey());
                        UsersHolder.getInstance().addUserToMap(newUser);
                        bus.post(new EventAddedUser());
                    }
                } else {
                    Log.i(TAG, "User: " + newUser.getName() + " is current user.");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class).setKey(dataSnapshot.getKey());

                Log.i(TAG, "User " + user.getName() + " moved");
                if(!user.getKey().equals(userKey)) {
                    UsersHolder.getInstance().updateUser(user);

                    bus.post(new EventUserLocationChanged(user));
                } else {
                    Log.i(TAG, "User: " + user.getName() + " is current user.");
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}
