package com.noisytask.locationtracker.utils;

/**
 * Holds applications constant values.
 */
public class Constants {

    public final static String FIREBASE_URL = "https://noisytask.firebaseio.com/";

    public final static String SP = "sharedPreferences";

    public final static String USERS = "users";

    public final static String EMAIL = "email";

    public final static String USER_LOCATION = "location";

    public final static String USER_KEY = "userKey";

    public final static int LOCATION_UPDATE_INTERVAL = 5000;

    public final static int LOCATION_UPDATE_DISTANCE = 10;

}
